$(function () {
  $(".hamburgar").on("click", function () {
    $(".nav").toggleClass("open");
    $(this).toggleClass("open");
  });

  $(".home_banner").slick({
    infinite: true,
    arrows: true,
    dots: true,
  });

  $(".scroll").mCustomScrollbar();
});

$(window).resize(function () {});
